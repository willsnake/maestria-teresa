<!DOCTYPE html>
<html lang="es">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Proyecto para la clase de maestria de la profesora Teresa Parra">
    <meta name="author" content="Lizbeth Luna Romero">
    <meta name="author" content="Daniel Rodríguez Monroy">

    <title>Proyecto de Maestria</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?= base_url("assets/css/bootstrap.min.css") ?>" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?= base_url("assets/css/stylish-portfolio.css") ?>" rel="stylesheet">

    <!-- Bootstrap Datepicker -->
    <link href="<?= base_url("assets/css/bootstrap-datepicker3.standalone.min.css") ?>" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?= base_url("assets/font-awesome/css/font-awesome.min.css") ?>" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">

</head>

<body data-ng-app="formulas" data-ng-controller="FormulasController">

<!-- Navigation -->
<a id="menu-toggle" href="#" class="btn btn-dark btn-lg toggle"><i class="fa fa-bars"></i></a>
<nav id="sidebar-wrapper">
    <ul class="sidebar-nav">
        <a id="menu-close" href="#" class="btn btn-light btn-lg pull-right toggle"><i class="fa fa-times"></i></a>
        <li class="sidebar-brand">
            <a href="#top">Proyecto Maestría</a>
        </li>
        <li>
            <a href="#about"><i class="fa fa-usd ic-menu"></i> Calcular Monto Simple</a>
        </li>
        <li>
            <a href="#services"><span>%</span> Calcular Interés Simple</a>
        </li>
    </ul>
</nav>

<!-- Header -->
<header id="top" class="header">
    <div class="text-vertical-center">
        <h1 style="font-size: 3em; width:40%; margin:0 auto;">Análisis de Costos y Toma de Decisiones en Sistemas Informáticos</h1>
        <br>
        <a href="#about" class="btn btn-dark btn-lg">¡Empecemos!</a>
    </div>
</header>

<!-- About -->
<section id="about" class="about">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h1 class="title1">Ingreso de Datos</h1>
                <h4 class="subtitle1">Introduce los datos correspondientes al cálculo que deseas realizar.</h4>

                <form name="forma_formulas" class="form-horizontal" id="forma_formulas">

                    <div class="alert alert-danger" role="alert" data-ng-show="forma_formulas.capital.$error.number">¡El valor ingresado no es un número!</div>

                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">$</div>
                            <input type="number" name="capital" data-ng-model="formulas.capital" class="form-control" placeholder="Capital" />
                        </div>
                    </div>

                    <div class="alert alert-danger" role="alert" data-ng-show="forma_formulas.monto.$error.number">¡El valor ingresado no es un número!</div>

                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">$</div>
                            <input type="number" name="monto" data-ng-model="formulas.monto" class="form-control" placeholder="Monto" />
                        </div>
                    </div>

                    <div class="alert alert-danger" role="alert" data-ng-show="forma_formulas.tasainteres.$error.number">¡El valor ingresado no es un número!</div>

                    <div class="form-group">
                        <div class="input-group">
                            <input type="number" name="tasainteres" data-ng-model="formulas.tasainteres" class="form-control" placeholder="Tasa de Interes" data-ng-blur="porcentaje($event)" />
                            <div class="input-group-addon">%</div>
                        </div>
                    </div>

                    <div class="alert alert-danger" role="alert" data-ng-show="forma_formulas.interes.$error.number">¡El valor ingresado no es un número!</div>

                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">$</div>
                            <input type="number" name="interes" data-ng-model="formulas.interes" class="form-control" placeholder="Interes Simple" />
                        </div>
                    </div>

                    <div class="alert alert-danger" role="alert" data-ng-show="forma_formulas.descuento.$error.number">¡El valor ingresado no es un número!</div>

                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">$</div>
                            <input type="number" name="descuento" data-ng-model="formulas.descuento" class="form-control" placeholder="Descuento" />
                        </div>
                    </div>

                    <div class="alert alert-danger" role="alert" data-ng-show="forma_formulas.tasadescuento.$error.number">¡El valor ingresado no es un número!</div>

                    <div class="form-group">
                        <div class="input-group">
                            <input type="number" name="tasadescuento" data-ng-model="formulas.tasadescuento" class="form-control" placeholder="Tasa de Descuento" data-ng-blur="porcentaje($event)" />
                            <div class="input-group-addon">%</div>
                        </div>
                    </div>

                    <div class="form-group">
                        <input type="text" name="tiempo" data-ng-model="formulas.tiempo" class="form-control" placeholder="Tiempo o Plazo" />
                    </div>
            </div>    
            <div class="col-lg-12 text-center tiempos">
                <h1 class="title1">Tiempo</h1>
                <div class="row">
                    <div class="col-lg-6">
                        <h2 class="title2">Cálulo por fechas</h2>
                        <div class="input-daterange input-group" id="datepicker">
                            <input type="text" class="input-sm form-control" name="fecha_inicio" data-ng-model="formulas.fecha_inicio" placeholder="Fecha de Inicio" />
                            <span class="input-group-addon">A</span>
                            <input type="text" class="input-sm form-control" name="fecha_fin" data-ng-model="formulas.fecha_fin" placeholder="Fecha de Fin" />
                        </div>
                         <div class="row">
                            <div class="col-lg-6">
                                 <h4>Tiempo Real</h4>
                                 <span data-ng-bind="tiempoReal()"></span> días
                            </div>
                            <div class="col-lg-6">
                                <h4>Tiempo Estimado</h4>
                                <span data-ng-bind="tiempoEstimado()"></span> días
                            </div>
                        </div>                       
                    </div>

                    <div class="col-lg-6">
                        <h3 class="title2">Suma o resta de días, meses y años</h3>

                    <div class="form-inline">
                        <div class="form-group">
                            <select name="operacion_fecha" data-ng-model="formulas.operacion_fecha" class="form-control">
                                <option>+</option>
                                <option>-</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" id="fecha_inicio_preciso" name="fecha_inicio_preciso" data-ng-model="formulas.fecha_inicio_preciso" placeholder="Fecha de Inicio" />
                        </div>
                    </div>

                   
                        <div class="form-group">
                            <input type="number" class="form-control" id="year_preciso" data-ng-model="formulas.year_preciso" placeholder="Años">
                        </div>
                        <div class="form-group">
                            <input type="number" class="form-control" id="meses_precisos" data-ng-model="formulas.meses_precisos" placeholder="Meses">
                        </div>
                        <div class="form-group">
                            <input type="number" class="form-control" id="dias_precisos" data-ng-model="formulas.dias_precisos" placeholder="Días">
                        </div>
                    

                    <h4>Fecha Final</h4>
                    <span data-ng-bind="calculoFecha()"></span>
                    </div>
                    
                </form>
            </div>
        </div>
    </div>
</section>

<section id="services" class="services bg-primary">
     <div  class="services-title text-center">
            <h1 class="title1">Fórmulas</h1>
            <h4 class="subtitle">En esta sección encontrarás los resultados aplicados a las fórmulas.</h4>
        </div>

    <div class="container">
        <div class="row text-center">
            <div id="carousel-example-generic" class="carousel slide">
                <ol class="carousel-indicators">
                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="3"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="4"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="5"></li>
                </ol>

                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-4">
                                    <img class="img-portfolio img-responsive" src="<?= base_url("assets/img/ecuaciones/monto_simple.png") ?>" style="padding-bottom: 20%;" />
                                    <div class="carousel-caption">
                                        Monto Simple = <span data-ng-bind="montoSimple() | currency"></span>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <img class="img-portfolio img-responsive" src="<?= base_url("assets/img/ecuaciones/capital_simple.png") ?>" style="padding-bottom: 20%;" />
                                    <div class="carousel-caption">
                                        Capital Simple = <span data-ng-bind="capitalSimpleDespeje() | currency"></span>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <img class="img-portfolio img-responsive" src="<?= base_url("assets/img/ecuaciones/interes_simple.png") ?>" style="padding-bottom: 20%;" />
                                    <div class="carousel-caption">
                                        Interés Simple = <span data-ng-bind="interesSimpleDespeje() | currency"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-3">
                                    <img class="img-portfolio img-responsive" src="<?= base_url("assets/img/ecuaciones/formula_interes1.png") ?>" style="padding-bottom: 25%;" />
                                    <div class="carousel-caption">
                                        Interés Simple = <span data-ng-bind="interesSimple() | currency"></span>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <img class="img-portfolio img-responsive" src="<?= base_url("assets/img/ecuaciones/formula_interes2.png") ?>" style="padding-bottom: 25%;" />
                                    <div class="carousel-caption">
                                        Capital Simple = <span data-ng-bind="capitalSimple() | currency"></span>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <img class="img-portfolio img-responsive" src="<?= base_url("assets/img/ecuaciones/formula_interes3.png") ?>" style="padding-bottom: 25%;" />
                                    <div class="carousel-caption">
                                        Tasa de Interes Simple = <span data-ng-bind="tasaSimple()"></span>%
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <img class="img-portfolio img-responsive" src="<?= base_url("assets/img/ecuaciones/formula_interes4.png") ?>" style="padding-bottom: 25%;" />
                                    <div class="carousel-caption">
                                        Tiempo o plazo Simple= <span data-ng-bind="tiempoSimple()"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-3">
                                    <img class="img-portfolio img-responsive" src="<?= base_url("assets/img/ecuaciones/formula_completa1.png") ?>" style="padding-bottom: 25%;" />
                                    <div class="carousel-caption">
                                        Monto = <span data-ng-bind="montoCompleto() | currency"></span>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <img class="img-portfolio img-responsive" src="<?= base_url("assets/img/ecuaciones/formula_completa2.png") ?>" style="padding-bottom: 25%;" />
                                    <div class="carousel-caption">
                                        Capital = <span data-ng-bind="capitalCompleto() | currency"></span>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <img class="img-portfolio img-responsive" src="<?= base_url("assets/img/ecuaciones/formula_completa3.png") ?>" style="padding-bottom: 25%;" />
                                    <div class="carousel-caption">
                                        Tiempo = <span data-ng-bind="tiempoCompleto()"></span>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <img class="img-portfolio img-responsive" src="<?= base_url("assets/img/ecuaciones/formula_completa4.png") ?>" style="padding-bottom: 25%;" />
                                    <div class="carousel-caption">
                                        Tasa de Interés = <span data-ng-bind="tasaCompleto() "></span>%
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-3">
                                    <img class="img-portfolio img-responsive" src="<?= base_url("assets/img/ecuaciones/descuento_real1.png") ?>" style="padding-bottom: 25%;" />
                                    <div class="carousel-caption">
                                        Monto = <span data-ng-bind="montoDescuentoReal() | currency"></span>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <img class="img-portfolio img-responsive" src="<?= base_url("assets/img/ecuaciones/descuento_real2.png") ?>" style="padding-bottom: 25%;" />
                                    <div class="carousel-caption">
                                        Capital = <span data-ng-bind="capitalDescuentoReal() | currency"></span>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <img class="img-portfolio img-responsive" src="<?= base_url("assets/img/ecuaciones/descuento_real3.png") ?>" style="padding-bottom: 25%;" />
                                    <div class="carousel-caption">
                                        Tiempo = <span data-ng-bind="tiempoDescuentoReal()"></span>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <img class="img-portfolio img-responsive" src="<?= base_url("assets/img/ecuaciones/descuento_real4.png") ?>" style="padding-bottom: 25%;" />
                                    <div class="carousel-caption">
                                        Tasa de Descuento = <span data-ng-bind="tasaDescuentoReal()">%</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-4">
                                    <img class="img-portfolio img-responsive" src="<?= base_url("assets/img/ecuaciones/comodin1.png") ?>" style="padding-bottom: 18%;" />
                                    <div class="carousel-caption">
                                        Monto = <span data-ng-bind="descuentoComodinMonto() | currency"></span>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <img class="img-portfolio img-responsive" src="<?= base_url("assets/img/ecuaciones/comodin2.png") ?>" style="padding-bottom: 18%;" />
                                    <div class="carousel-caption">
                                        Capital = <span data-ng-bind="descuentoComodinCapital() | currency"></span>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <img class="img-portfolio img-responsive" src="<?= base_url("assets/img/ecuaciones/comodin3.png") ?>" style="padding-bottom: 18%;" />
                                    <div class="carousel-caption">
                                        Descuento = <span data-ng-bind="descuentoComodinDescuento() | currency"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-12">
                                    <img class="img-portfolio img-responsive" src="<?= base_url("assets/img/ecuaciones/descuento_comercial2.png") ?>" style="padding-bottom: 5%;" />
                                    <div class="carousel-caption">
                                        Descuento = <span data-ng-bind="descuentoCompletoComercial() | currency"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-3">
                                    <img class="img-portfolio img-responsive" src="<?= base_url("assets/img/ecuaciones/descuento_comercial1.png") ?>" style="padding-bottom: 25%;" />
                                    <div class="carousel-caption">
                                        Descuento = <span data-ng-bind="Descuento() | currency"></span>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <img class="img-portfolio img-responsive" src="<?= base_url("assets/img/ecuaciones/descuento_comercial3.png") ?>" style="padding-bottom: 25%;" />
                                    <div class="carousel-caption">
                                        Monto = <span data-ng-bind="montoDescuentoComercial() | currency"></span>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <img class="img-portfolio img-responsive" src="<?= base_url("assets/img/ecuaciones/descuento_comercial5.png") ?>" style="padding-bottom: 25%;" />
                                    <div class="carousel-caption">
                                        Tiempo = <span data-ng-bind="tiempoDescuentoComercial()"></span>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <img class="img-portfolio img-responsive" src="<?= base_url("assets/img/ecuaciones/descuento_comercial4.png") ?>" style="padding-bottom: 25%;" />
                                    <div class="carousel-caption">
                                        Tasa de Descuento = <span data-ng-bind="tasaDescuentoComercial()">%</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>

<!-- jQuery -->
<script src="<?= base_url("assets/js/jquery.min.js") ?>"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<?= base_url("assets/js/bootstrap.min.js") ?>"></script>

<!-- AngularJS Core JavaScript -->
<script src="<?= base_url("assets/js/angular.min.js") ?>"></script>

<!-- Funciones de Angular -->
<script src="<?= base_url("assets/js/efectosAngular.js") ?>"></script>

<!-- Bootstrap Datepicker -->
<script src="<?= base_url("assets/js/bootstrap-datepicker.min.js") ?>"></script>
<script src="<?= base_url("assets/locales/bootstrap-datepicker.es.min.js") ?>"></script>

<!-- Custom Theme JavaScript -->
<script>
    // Closes the sidebar menu
    $("#menu-close").click(function(e) {
        e.preventDefault();
        $("#sidebar-wrapper").toggleClass("active");
    });

    // Opens the sidebar menu
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#sidebar-wrapper").toggleClass("active");
    });

    // Scrolls to the selected menu item on the page
    $(function() {
        $('a[href*=#]:not([href=#])').click(function() {
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') || location.hostname == this.hostname) {

                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    $('html,body').animate({
                        scrollTop: target.offset().top
                    }, 1000);
                    return false;
                }
            }
        });
    });
</script>

<script>
    $('.input-daterange').datepicker({
        language: 'es'
    });
    $('#fecha_inicio_preciso').datepicker({
        language: 'es'
    });
</script>

</body>

</html>