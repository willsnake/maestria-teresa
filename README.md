# README #

Proyecto de Maestría para la clase de la maestra Teresa Parra

### ¿Para que es este desarrollo? ###

* Este desarrollo está hecho en PHP y AngularJS y sirve para calcular algunas fórmulas de contabilidad
* Versión 1.0

### Pasos para instalación ###

* Requerimientos
* Configuración de instalación

### Requerimientos ###

* PHP 5.5 o mayor
* Servidor web

### Configuración de instalación ###

Se crea una carpeta dentro del servidor web con el nombre que queramos.
Se tiene que modificar la línea del archivo `application/config/config.php` número 17:
`'http://localhost/MaestriaTeresa/'`
Por el lugar donde copiamos el desarrollo.
Eso es todo lo que se tiene que cambiar